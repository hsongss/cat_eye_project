package com.hp.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hp.pojo.Admin;

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

	@Select("SELECT * FROM admin WHERE a_name=#{aName} AND a_pwd=#{aPwd}")
	Admin getAdmin(String aName, String aPwd);
	
	
	
	Admin findByName(String aName);

	Admin findByaId(Integer aId);

}
