 package com.hp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hp.pojo.BasePojo;

/**
  * 公共类的接口
  * @author zx
  * @date 2019/12/25
  */
 public interface BaseMappers extends BaseMapper<BasePojo> {

     /**
      * 通过电影Id查找相关信息
      */
     @Select("select m.m_film,m.m_score,m.m_time,m.m_type,m.m_version,m.m_star,d.d_day,st.s_time,st.s_endTime,ch.h_name,ch.h_address,vh.v_name,t.m_price from movie m" + 
         "left join movie_day md on m.m_id=md.mov_id" + 
         "left join `day` d on d.d_id=md.day_id" + 
         "left join day_showtime ds on ds.day_id=d.d_id" + 
         "left join show_time st on st.s_id=ds.show_time_id" + 
         "left join movie_cathouse mc on mc.mov_id=m.m_id" + 
         "left join cat_house ch on ch.h_id=mc.vh_id" + 
         "left join videohall_cathouse vc on vc.ch_id=ch.h_id" + 
         "left join video_hall vh on vh.v_id=vc.vh_id" + 
         "left join movie_ticket mt on m.m_id=mt.mov_id" + 
         "left join ticket t on t.t_id=mt.tic_id" + 
         "where m.m_id=#{mId}"
         )
     List<BasePojo> getBaseInfoByMId(@Param("mId") Integer mId);
  
     /**
      * 查询影厅
      * @return
      */
     @Select("select DISTINCT st.s_time,st.s_endTime,m.m_version,vh.v_name,m.m_price from movie m\r\n" + 
         "left join movie_day md on m.m_id=md.mov_id\r\n" + 
         "left join `day` d on d.d_id=md.day_id\r\n" + 
         "left join day_showtime ds on ds.day_id=d.d_id\r\n" + 
         "left join show_time st on st.s_id=ds.show_time_id\r\n" + 
         "left join movie_cathouse mc on mc.mov_id=m.m_id\r\n" + 
         "left join cat_house ch on ch.h_id=mc.vh_id\r\n" + 
         "left join videohall_cathouse vc on vc.ch_id=ch.h_id\r\n" + 
         "left join video_hall vh on vh.v_id=vc.vh_id\r\n" + 
         "where m.m_id=#{mId}")
     List<BasePojo> selectMovieInfo(@Param("mId") Integer mId);
}
