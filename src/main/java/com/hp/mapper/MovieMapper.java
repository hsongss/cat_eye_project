package com.hp.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hp.pojo.Movie;

@Mapper
public interface MovieMapper extends BaseMapper<Movie> {

}
