 package com.hp.mapper;

import org.apache.ibatis.annotations.Select;
import org.mybatis.spring.annotation.MapperScan;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hp.pojo.User;

/**
 * @author 76184
 * @date 2019/12/13
 */
@MapperScan
public interface UserMapper extends BaseMapper<User>{

    /**
     * @param uPhone
     * @param uPassWord
     * @return
     */
    @Select("select * from user where u_phone=#{uPhone} and u_pass_word=#{uPassWord}")
    User selectUser(String uPhone, String uPassWord);
    
    
    
    
}
