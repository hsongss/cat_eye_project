 package com.hp.mapper;




import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hp.pojo.Movie;

public interface MovieListMapper extends BaseMapper<Movie> {
	/**
	 * 通过id查询电影
	 * @param mId
	 * @return
	 */
	@Select("SELECT * FROM movie WHERE m_id=#{mId} ")
	Movie selectById(Integer mId);
	/**
	 * 后台条件查询
	 * @param movie
	 * @return m_type like ${mType}%
	 */
//	@Select("SELECT * FROM movie where m_film LIKE '%${movie.mFilm}%' and  m_type LIKE '%${movie.mType}%'")
//	@Select("SELECT * FROM movie where m_film LIKE concat('%',#{movie.mFilm},'%') and"
//			+ "  m_type LIKE concat('%',#{movie.mType},'%')")
	IPage<Movie> selectPages(Page<Movie> page2, Movie movie);

}
