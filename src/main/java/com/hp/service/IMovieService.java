package com.hp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.Movie;

public interface IMovieService extends IService<Movie> {
		
}
