package com.hp.service.imp;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hp.mapper.AdminMapper;
import com.hp.mapper.UserMapper;
import com.hp.pojo.Admin;
import com.hp.service.IAdminService;

/**
 * 管理员登录方法
 * 
 * @author SONGSS
 *
 */
@Service
@Transactional(readOnly = true)
public class AdminService extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

	@Resource
	private AdminMapper adminMapper;

	
	@Override
	public Admin getAdmin(String aName, String aPwd) {

		return adminMapper.getAdmin(aName, aPwd);
	}
	
	
	@Override
	public Admin findByName(String aName) {
		
		return adminMapper.findByName(aName);
	}

	@Override
	public Admin findByaId(Integer aId) {
		
		return adminMapper.findByaId(aId);
	}

	

}
