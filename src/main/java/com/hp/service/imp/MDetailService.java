 package com.hp.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hp.mapper.IMDetailMapper;
import com.hp.pojo.Movie;
import com.hp.service.IMDetailService;

@Service
@Transactional(readOnly=true)
public class MDetailService implements IMDetailService{

    @Resource
    private IMDetailMapper mDetailMapper;
  
    /**
     * 查询所有电影
     */
    @Override
    public List<Movie> selectMovies() {
         return mDetailMapper.selectList(null);
    }

}
