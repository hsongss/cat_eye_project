package com.hp.service.imp;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hp.mapper.MovieMapper;
import com.hp.pojo.Movie;
import com.hp.service.IMovieService;

@Service
@Transactional(readOnly = true)
public class MovieService extends ServiceImpl<MovieMapper, Movie> implements IMovieService {

	@Resource
	private MovieMapper movieMapper;
}
