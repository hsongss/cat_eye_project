package com.hp.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hp.mapper.CatHouseMapper;
import com.hp.mapper.MovieListMapper;
import com.hp.pojo.CatHouse;
import com.hp.pojo.CatHouseVo;
import com.hp.pojo.Movie;
import com.hp.pojo.MovieVo;
import com.hp.service.ICatHouseService;
import com.hp.service.IMovieListService;



@Service
@Transactional(readOnly = true)
//public class MovieListService implements IMovieListService{
public class CatHouseService extends ServiceImpl<CatHouseMapper, CatHouse> implements ICatHouseService {
	@Resource
	private CatHouseMapper catHouseMapper;

	/**
	 * 所有电影进行查询并分页
	 */
	public CatHouseVo selectPage(Integer page, Integer size) {
        Page<CatHouse> page2 = new Page<>(page,size);
        IPage<CatHouse> selectPage = catHouseMapper.selectPage(page2, null);
        CatHouseVo vo = new CatHouseVo();
        vo.setCode("0");
        vo.setCount(selectPage.getTotal());
        vo.setMsg("");
        vo.setData(selectPage.getRecords());
        return vo;
    }
	
	/**
	 * 后台条件查询
	 * 
	 * @param movie
	 * @return
	 */
	@Override
	public CatHouseVo selectCatHouse(CatHouse catHouse,Integer page, Integer size) {
		
		Page<CatHouse> page2 = new Page<>(page, size);
		
		IPage<CatHouse> selectPage = catHouseMapper.selectPages(page2, catHouse);
		CatHouseVo vo = new CatHouseVo();
		vo.setData(selectPage.getRecords());
		vo.setPages((int) selectPage.getPages());
		return vo;
	}

	

	/**
	 * 添加电影
	 */
//	@Transactional(propagation = Propagation.REQUIRED)
//	public Integer insertMovie(CatHouse catHouse) {
//
//		int insert = catHouseMapper.insert(catHouse);
//		return insert;
//	}

	/**
	 * 通过id查询电影
	 */
//	@Override
//	public CatHouse selectById(Integer hId) {
//
//		return catHouseMapper.selectById(hId);
//	}

	

}
