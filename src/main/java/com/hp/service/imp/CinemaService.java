 package com.hp.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hp.mapper.ICinemaMapper;
import com.hp.pojo.CatHouse;
import com.hp.service.ICinemaService;

@Service
@Transactional(readOnly=true)
public class CinemaService implements ICinemaService{

    @Resource
    private ICinemaMapper cinemaMapper;
    
    /**
     * 查询影院信息
     * @return
     */
    
    public List<CatHouse> selectCatHouse(){
        
        return cinemaMapper.selectList(null);
        
    }
}
