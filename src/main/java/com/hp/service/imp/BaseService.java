 package com.hp.service.imp;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hp.mapper.BaseMappers;
import com.hp.pojo.BasePojo;
import com.hp.service.IBaseService;

/**
 * 
 * @author zx
 * @date 2019/12/25
 */
@Service
@Transactional(readOnly=true)
public class BaseService extends ServiceImpl<BaseMappers,BasePojo> implements IBaseService{

    @Resource
    private BaseMappers baseMapper;
    
    /**
     * 通过电影Id查找相关信息
     */
    @Override
    public List<BasePojo> getBaseInfoByMId(Integer mId) {
         return baseMapper.getBaseInfoByMId(mId);
    }

    @Override
    public List<BasePojo> selectMovieInfo(Integer mId) {
        // TODO Auto-generated method stub
         return baseMapper.selectMovieInfo(mId);
    }

}
