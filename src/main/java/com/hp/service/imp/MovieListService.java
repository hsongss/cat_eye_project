package com.hp.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hp.mapper.MovieListMapper;
import com.hp.pojo.Movie;
import com.hp.pojo.MovieVo;
import com.hp.service.IMovieListService;



@Service
@Transactional(readOnly = true)
//public class MovieListService implements IMovieListService{
public class MovieListService extends ServiceImpl<MovieListMapper, Movie> implements IMovieListService {
	@Resource
	private MovieListMapper movieListMapper;

	/**
	 * 所有电影进行查询并分页
	 */
	public MovieVo selectPage(Integer page, Integer size) {
        Page<Movie> page2 = new Page<>(page,size);
        IPage<Movie> selectPage = movieListMapper.selectPage(page2, null);
        MovieVo vo = new MovieVo();
        vo.setCode("0");
        vo.setCount(selectPage.getTotal());
        vo.setMsg("");
        vo.setData(selectPage.getRecords());
        return vo;
    }
	
	/**
	 * 后台条件查询
	 * 
	 * @param movie
	 * @return
	 */
	@Override
	public MovieVo selectMovies(Movie movie,Integer page, Integer size) {
		
		Page<Movie> page2 = new Page<>(page, size);
		
		IPage<Movie> selectPage = movieListMapper.selectPages(page2, movie);
		MovieVo vo = new MovieVo();
		vo.setData(selectPage.getRecords());
		vo.setPages((int) selectPage.getPages());
		return vo;
	}

	/**
	 * 多条件查询
	 */
	public List<Movie> selectMoviesBymAreaAnd(String mType, String mArea, String bTime, Integer id) {
		QueryWrapper<Movie> list = new QueryWrapper<Movie>();
		if (id == 1) {
			list.like("m_area", "%" + mArea + "%").like("m_type", "%" + mType + "%").like("m_year", "%" + bTime + "%")
					.orderByDesc("m_boxoff");
		} else if (id == 2) {
			list.like("m_area", "%" + mArea + "%").like("m_type", "%" + mType + "%").like("m_year", "%" + bTime + "%")
					.orderByDesc("m_show");
		} else if (id == 3) {
			list.like("m_area", "%" + mArea + "%").like("m_type", "%" + mType + "%").like("m_year", "%" + bTime + "%")
					.orderByDesc("m_score");
		}

		list.like("m_area", "%" + mArea + "%").like("m_type", "%" + mType + "%").like("m_year", "%" + bTime + "%");
		return movieListMapper.selectList(list);
	}

	/**
	 * 添加电影
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public Integer insertMovie(Movie movie) {

		int insert = movieListMapper.insert(movie);
		return insert;
	}

	/**
	 * 通过id查询电影
	 */
	@Override
	public Movie selectById(Integer mId) {

		return movieListMapper.selectById(mId);
	}

	

}
