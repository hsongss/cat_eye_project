 package com.hp.service.imp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hp.mapper.UserMapper;
import com.hp.pojo.User;
import com.hp.service.IUserService;

/**
 * @author 76184
 * @date 2019/12/13
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> implements IUserService{

    @Resource
    private UserMapper userMapper;
    
    /**
     * 注册
     */
    public int register(User user) {
         return userMapper.insert(user);
    }

    /** 
     * 登陆
     */
    public User singnIn(String uPhone,String uPassWord) {
        return userMapper.selectUser(uPhone,uPassWord);
    }
}
