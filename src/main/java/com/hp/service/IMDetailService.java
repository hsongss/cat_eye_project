 package com.hp.service;

import java.util.List;

import com.hp.pojo.Movie;

public interface IMDetailService {

     /**
      * 查询所有电影
      * @return
      */
    List<Movie> selectMovies();
    
}
