 package com.hp.service;

import java.util.List;

import com.hp.pojo.CatHouse;

public interface ICinemaService {

    /**
     * 查询影院信息
     * @return
     */
     List<CatHouse> selectCatHouse();
}
