 package com.hp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.User;

/**
 * @author 76184
 * @date 2019/12/13
 */
public interface IUserService extends IService<User>{
    
    /**
     * 注册
     */
    int register(User user);
    /**
     * 登陆
     */
    User singnIn(String uPhone,String uPassWord);
}
