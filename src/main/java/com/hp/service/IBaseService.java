 package com.hp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.BasePojo;

public interface IBaseService extends IService<BasePojo>{

    /**
     * 通过电影Id查找相关信息
     */
     List<BasePojo> getBaseInfoByMId(Integer mId);
     
    List<BasePojo> selectMovieInfo(Integer mId);
}
