 package com.hp.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.CatHouse;
import com.hp.pojo.CatHouseVo;
import com.hp.pojo.Movie;
import com.hp.pojo.MovieVo;

public interface ICatHouseService extends IService<CatHouse>{
//public interface IMovieListService{
	
	
	/**
     * 
     * @param pageNo 当前页码
     * @param pageSize 当前页码显示条数
     * @return
     */
	CatHouseVo selectPage(Integer page, Integer limit);
	
//	CatHouse selectById(Integer hId);


   
 

    
    /**
     * 后台模糊查询
     * @param movie
     * @param page
     * @param sizes
     * @return
     */
    CatHouseVo selectCatHouse(CatHouse catHouse,Integer page, Integer sizes);
	
}
