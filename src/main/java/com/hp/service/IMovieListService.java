 package com.hp.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.Movie;
import com.hp.pojo.MovieVo;

public interface IMovieListService extends IService<Movie>{
//public interface IMovieListService{
	
	
//	/**
//     * 分页查询
//     */
//	List<Movie> selectAll(String mType, String mArea, Date bTime,Integer page, Integer size);
	/**
     * 
     * @param pageNo 当前页码
     * @param pageSize 当前页码显示条数
     * @return后台查询所有电影进行分页
     */
	MovieVo selectPage(Integer page, Integer limit);
	
    Movie selectById(Integer mId);

    /**
     * 添加电影
     */
    Integer insertMovie(Movie movie);
   
    /**
     * 多条件查询
     */
    List<Movie> selectMoviesBymAreaAnd(String mType,String mArea,String bTime,Integer id);

    
//    List<Map<?,?>> selectMovies(Movie movie);
    /**
     * 后台模糊查询
     * @param movie
     * @param page
     * @param sizes
     * @return
     */
    MovieVo selectMovies(Movie movie,Integer page, Integer sizes);
	
}
