package com.hp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hp.pojo.Admin;

public interface IAdminService extends IService<Admin> {

	/**
	 * 管理员登录验证 admin
	 * 
	 * @param admin
	 * @return
	 */
	Admin getAdmin(String aName, String aPwd);
	
	Admin findByName(String aName);
	Admin findByaId(Integer aId);
	
}
