 package com.hp.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

/**
  * 电影院类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class CatHouse {
	@TableId(type = IdType.AUTO)
    private Integer hId;    //序号
    private String hName;   //电影院名字
    private String hAddress;    //电影院地址
    private Double hPrice;    //
}
