 package com.hp.pojo;

import java.util.Date;

import lombok.Data;

/**
  * 评论类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Coment {

    private Integer cId;    //序号
    private Integer aId;    //账户id
    private Date cTime; //发表时间
    private String Content; //内容
}
