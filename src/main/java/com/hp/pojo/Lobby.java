 package com.hp.pojo;

import lombok.Data;

/**
  * 影厅类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Lobby {

    private Integer iId;    //序号
    private String iHall;   //几号厅
    private Integer hRow;   //牌号
    private Integer hLine;  //列号
    private Integer hNum;   //座位号
    private Integer hId;    //序号(外键-影院序号)
}
