 package com.hp.pojo;

import java.util.Date;

import lombok.Data;

/**
 * 放映时间类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class ShowTime {

    private Integer sId;    //序号
    private Date sTime; //放映时间
    private Date sEndTime;  //散场时间
    private Integer mId;    //序号(外键-电影序号)
    
}
