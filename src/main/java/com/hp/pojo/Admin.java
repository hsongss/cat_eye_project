package com.hp.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

/**
 * 管理员
 * 
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Admin {
	@TableId(type = IdType.AUTO)
	private Integer aId; // 序号
	private String aName; // 用户名
	private String aPwd; // 密码
	private String aPerms;
}
