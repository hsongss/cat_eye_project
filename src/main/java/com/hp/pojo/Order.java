 package com.hp.pojo;

import java.util.Date;

import lombok.Data;

/**
  * 订单类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Order {

    private Integer oId;    //序号
    private Integer oNum;   //取票码
    private Date oComeTime; //出票时间
}
