package com.hp.pojo;

import lombok.Data;

/**
 * 用户表
 * 
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class User {

    private Integer uId; // 序号
    private String uPhone; // 手机号
    private String uPassWord; // 密码

}
