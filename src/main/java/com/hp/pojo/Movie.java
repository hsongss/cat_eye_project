package com.hp.pojo;




import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

/**
 * 电影类
 * 
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Movie {
	@TableId(type = IdType.AUTO)
	private Integer mId; // 序号111
	private String mFilm; // 影片名111
	private String mPic; // 图片
	private Double mScore; // 评分111
	private String mType; // 类型
	private String mVersion; // 语言版本11
	private String mTime; // 总时长11
	private String mEnglish; // 影片英文名
	private String mYear;//年代
	private String mArea;//区域类型
	
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JSONField(format = "yyyy-MM-dd")
    private java.util.Date mShow;//上映时间11
    private Double mBoxoff;//票房111
    private String mStar;//主演
    private Integer mState;
    private Double mPrice;//票价
}






