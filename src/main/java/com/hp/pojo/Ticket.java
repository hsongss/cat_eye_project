 package com.hp.pojo;

import lombok.Data;

/**
 * 电影票类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Ticket {

     private Integer tId;   //序号
     private double mPrice; //单价
     private Integer mCount;    //数量
     private Integer mId;   //序号(外键-电影序号)
}
