package com.hp.pojo;

import java.util.List;

import lombok.Data;

@Data
public class MovieVo {
	 /**
     * 返回码  值为0表示查询成功
     */
    private String code;
    /**
     * 
     */
    private String msg;
    /**
     * 总条数
     */
    private long count=0;
    /**
     * 返回总条数
     */
    private List<Movie> data;
    /**
     * 总页数
     */
    private int pages;

}
