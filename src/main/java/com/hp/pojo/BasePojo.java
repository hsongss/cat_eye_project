 package com.hp.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
  * 公共实体类
  * @author zx
  * @date 2019/12/24
  */
@Data
 public class BasePojo {

     private String mFilm;//电影名
     private Integer mScore;//电影评分
     private String mTime;//电影时长
     @DateTimeFormat(pattern = "HH:mm:ss")
     @JSONField(format = "HH:mm:ss")
     private Date day;//观影时间
     private String mType;//电影类型
     private String mVersion;//语言版本1111
     private String mStar;//主演
     @DateTimeFormat(pattern = "HH:mm:ss")
     @JSONField(format = "HH:mm:ss")
     private Date sTime;//放映时间11111
     @DateTimeFormat(pattern = "HH:mm:ss")
     @JSONField(format = "HH:mm:ss")
     private Date sEndTime;//散场时间
     private String hName;//影院名
     private String hAddress;//影院地址
     private String vName;//影厅名11111
     private Double mPrice;//票价111
     
}
