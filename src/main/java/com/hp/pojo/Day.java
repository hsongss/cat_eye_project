 package com.hp.pojo;

import java.util.Date;

import lombok.Data;

/**
 * 观影时间类
 * @author 76184
 * @date 2019/12/13
 */
@Data
public class Day {

    private Integer dId;    //序号
    private Date day;   //观影日期
    private Integer mId;    //序号(外键-电影序号)
}
