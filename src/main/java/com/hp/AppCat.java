package com.hp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.hp")
public class AppCat {

	public static void main(String[] args) {
		SpringApplication.run(AppCat.class, args);
	}

}
