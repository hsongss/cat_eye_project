package com.hp.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hp.pojo.CatHouse;
import com.hp.pojo.CatHouseVo;
import com.hp.pojo.Movie;
import com.hp.pojo.MovieVo;
import com.hp.pojo.User;
import com.hp.service.ICatHouseService;
import com.hp.service.IMovieListService;
import com.hp.service.IUserService;




@Controller
 public class CatHouseController {
   
    @Resource
    private ICatHouseService catHouseService;
    
    
    
    
    /**
	 * 查询所有电影信息并分页
	 * @return
	 */
//	@RequestMapping(value="movieList")
//	@ResponseBody
//	public Object ListUsers(@RequestParam(defaultValue="2")Integer page,@RequestParam(defaultValue="1")Integer limit){
//		MovieVo selectPage = movieListService.selectPage(page, limit);
//		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("code", 0);
//		jsonObject.put("data", selectPage.getData());
//		jsonObject.put("msg", "");
//		jsonObject.put("count", selectPage.getPages());
//		System.err.println(selectPage.getData());
//        return JSON.toJSONString(jsonObject); 
//	}
    /**
	 * 查询所有电影信息并分页
	 * @return
	 */
	@RequestMapping(value="CatHouseList")
	@ResponseBody
	public Object CatHouseList(@RequestParam(defaultValue="1")Integer page,@RequestParam(defaultValue="10")Integer limit){
		CatHouseVo vo = (CatHouseVo)catHouseService.selectPage(page, limit);
        return JSON.toJSONString(vo); 
	}
	/**
	 * 后台条件查询
	 * @param movie
	 * @return
	 */
	@RequestMapping(value = "/selectCatHouses")
	@ResponseBody
	public String selectMovie(CatHouse catHouse,Integer page,Integer limit){
//		public String selectMovie(Movie movie,@RequestParam(defaultValue="3")Integer page,@RequestParam(defaultValue="1")Integer limit){
//		movie.setmFilm("两只老虎");
//		movie.setmType("全部");
		System.err.println(catHouse);
		System.err.println(page);
		System.err.println(limit);
		CatHouseVo selectMovies = catHouseService.selectCatHouse(catHouse, page, limit);
		JSONObject jsonObject = new JSONObject();
////		code = 0, msg = "", count = counts, data = listData
		jsonObject.put("code", 0);
		jsonObject.put("data", selectMovies.getData());
		jsonObject.put("msg", "");
		jsonObject.put("count", selectMovies.getPages());
		
		System.err.println(selectMovies.getData());
		return  JSON.toJSONString(jsonObject);
		
	}
    /**
     * 通过多个属性
     */
//    @RequestMapping(value="manypar")
//    @ResponseBody
//    public String selectMoviesBymArea(String mArea,String mType,String bTime,String mScore,Integer id) {
//        List<Movie> manypar = movieListService.selectMoviesBymAreaAnd(mType, mArea, bTime,id);
//        
//        return JSON.toJSONString(manypar);  
//    }

    
    /**
     * 添加电影
     */
	@RequestMapping(value = "insertcatHouse")
	public String insertMovie(CatHouse catHouse) {

		boolean insertMovie = catHouseService.save(catHouse);

		return "page/moviehall";
	}

    
   /**
    * 删除电影
    */
  @RequestMapping(value="/deletecatHouse")
  @ResponseBody
  public boolean deletetMovie(Integer hId) {
	  System.out.println(hId);
	   boolean deletetMovie = catHouseService.removeById(hId);
	   System.out.println(deletetMovie);
		return deletetMovie;
	}
    
	  /**
	   * 修改电影
	   */
//	 @RequestMapping(value="/updateMovie")
//	 @ResponseBody
//	 public boolean updatetMovie(Movie movie,Integer mId) {
//		  System.out.println(mId);
//		 
//		    boolean updateMovie = movieListService.updateById(movie);
//		   
//			return updateMovie;
//		}
//	 
	 
	 /**
	 * 查询电影通过id
	 */
//	@RequestMapping(value = "/selectMovie")
//	@ResponseBody
//	public Movie selectMoviesById(Integer mId, ModelMap model) {
//
//		Movie movie = movieListService.getById(mId);
//		model.put("movieedit", "movieedit");
//		return movie;
//	}
	
    
}
