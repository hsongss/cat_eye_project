package com.hp.controller;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hp.pojo.Admin;
import com.hp.service.IAdminService;

/**
 * 管理员登陆
 * 
 * @author SONGSS
 *
 */
@Controller
//@RequestMapping("adminlogin")
public class AdminController {

	@Resource
	private IAdminService adminService;
	
	@RequestMapping(value="/index")
	public String selectUser(Model model){
		
		model.addAttribute("name", "aaa");
		return "indexs";
	}
	
	
	
	
	@RequestMapping(value="/login")
	public String login(String name,String passWord,Model model){
		/**
		 * 使用shiro编写认证操作
		 */
		//1.获取subject
		Subject subject = SecurityUtils.getSubject();
		
		//2.封装用户数据
		UsernamePasswordToken token = new UsernamePasswordToken(name, passWord);
		try {
			subject.login(token);
			//登陆成功
			//跳转到index.html
			return "redirect:/indexs";
		} catch (UnknownAccountException e) {
			//登录失败:用户名不存在
			model.addAttribute("msg", "用户名不存在");
			return "login";
		} catch (IncorrectCredentialsException e) {
			//登录失败:密码错误
			model.addAttribute("msg", "密码错误");
			return "login";
		}
		
		
	}

	
	
	@RequestMapping(value="/toLogin")
	public String toLogin(){
		
		return "/login";
	}
	
	
	
	/**
	 * admin登录
	 */
	@RequestMapping(value = "dl")
	@ResponseBody
	public Admin adminlog(String aName,String aPwd) {
		Admin admin = adminService.getAdmin(aName, aPwd);
		return admin;
//		if(admin==null) {
//			return "error";
//		}else {
//			
//			return "success";
//		}
	}

	/**
	 * admin登录
	 */
	@RequestMapping(value = "adminlogin")
	public String adminlogin() {
		System.out.println("管理员需要登陆");
		return "/admin/index/login";
	}

	@RequestMapping(value = "admin")
	public String admin(Admin admin, ModelMap model) {

		String aName = admin.getAName();
		String aPwd = admin.getAPwd();

//		QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
//		queryWrapper.eq("a_name", aName);
//		queryWrapper.eq("a_pwd", aPwd);
//		Admin one = adminService.getOne(queryWrapper);

		Admin adminLogin = adminService.getAdmin(aName, aPwd);

		if (adminLogin == null) {
			return "/admin/index/login";
		} else {
			model.addAttribute("aName", aName);
			return "/admin/index/index";
		}
	}

}
