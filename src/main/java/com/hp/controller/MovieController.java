package com.hp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.hp.pojo.Movie;
import com.hp.service.IMovieService;

@Controller
@RequestMapping("/movie")
public class MovieController {
//	@Autowired
//	private IMovieService movieService;
//	@RequestMapping("/findAll")
//	@ResponseBody
//	public List<Movie> showimg() {		
//		List<Movie> selectList = movieService.selectList(null);
//		
//		
//		return selectList;
//	}
	@Autowired
	private IMovieService movieService;

	@RequestMapping("/img1")
	@ResponseBody
	public String geImg() {
		List<Movie> selectImg = movieService.list();
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> map = null;
		for (Movie movie : selectImg) {
			map = new HashMap<String, String>();
			map.put("id", movie.getMId() + "");
			map.put("img", movie.getMPic());
			list.add(map);
		}
		return JSON.toJSONString(list);
	}

	@RequestMapping("/img2")
	@ResponseBody
	public String geImg1() {
		List<Movie> selectImg = movieService.list();
		List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
		Map<String, String> map2 = null;
		for (Movie movie : selectImg) {
			map2 = new HashMap<String, String>();
			map2.put("id", movie.getMId() + "");
			map2.put("img", movie.getMPic());
			list2.add(map2);
		}

		return JSON.toJSONString(list2);
	}

	/**
	 * 电影详细页面
	 */
	@RequestMapping(value = "/selectMovie")
	public String selectMovie(ModelMap map, Integer id) {
		Movie selectById = movieService.getById(id);
		 map.put("selectById",selectById);

		return "/detail";
	}

}
