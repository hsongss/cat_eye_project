 package com.hp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
  * 选座
  * @author 赵鑫
  * @date 2019/12/21
  */
 @Controller
 public class MSiteController {

     @GetMapping("/mSite")
     public String mSite() {
         
        return "mSite";
         
     }
     
}
