package com.hp.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hp.pojo.User;
import com.hp.service.IUserService;

/**
 * @author 76184
 * @date 2019/12/13
 */
@Controller
@RequestMapping("login")
public class UserController {

	@Resource
	private IUserService userService;

	/**
	 * 注册
	 * 
	 * @param uId
	 * @param uPassWord
	 * @return
	 * @return
	 */
	@RequestMapping("/")
	public String Index() {
		return "index";
	}

	@RequestMapping("/login")
	public String login() {
		return "signIn";
	}

	@RequestMapping("/reg")
	public String reg() {
		return "regist";
	}

	@RequestMapping("/register")
	public String register(User user, ModelMap model) {

		String uPhone = user.getUPhone();
		System.out.println("手机号：" + uPhone);

		String uPassWord = user.getUPassWord();
		System.out.println("密码：" + uPassWord);

		int login = userService.register(user);

		if (login == 1) {

			return "signIn";
		}
		return "regist";

	}

	/**
	 * 登陆
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/sign")
	public String signIn(User user, ModelMap model) {
		String uPhone = user.getUPhone();
		String uPassWord = user.getUPassWord();
		User singnIn = userService.singnIn(uPhone, uPassWord);
		System.out.println(singnIn);
		if (singnIn == null) {
			return "signIn";
		} else {
			model.addAttribute("uPhone", uPhone);
			return "index";
		}

	}

}
