 package com.hp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("index")
 public class HelloController {

    @RequestMapping("index")
    public String hello(Model model) {
        
        model.addAttribute("msg", "hello world");
        System.out.println("111");
        return "hello";
    }
}
