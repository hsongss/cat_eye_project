package com.hp.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.hp.pojo.BasePojo;
import com.hp.service.IBaseService;

/**
 * 影院详情
 * 
 * @author 赵鑫
 * @date 2019/12/21
 */
@Controller
public class MDetailController {

    @Resource
    private IBaseService baseService;

    /**
     * 进入影院详情
     * 
     * @return
     */
    @RequestMapping(value="mDetail")
    public String mDetail(Model model) {

        return "mDetail";
    }
    
   /* *//**
     * 遍历电影
     * 
     * @return
     *//*
    @RequestMapping(value = "movies")
    @ResponseBody
    public List<Movie> selectMoviess(ModelMap map) {

        List<Movie> movies = mDetailService.selectMovies();

         map.put("movies", movies);
        System.out.println(movies);
        return movies;
    }*/
    
    @RequestMapping("movieInfo")
    @ResponseBody
    public String selectMovieInfo(ModelMap map,Integer id) {
         
        List<BasePojo> movieHall = baseService.selectMovieInfo(id);
        map.put("movieHall", movieHall);
        System.out.println(movieHall);
        return JSON.toJSONString(movieHall);
         
     }

    /**
      * 遍历影院
      *//*
     @RequestMapping(value="catHouse")
     @ResponseBody
     public List<CatHouse> selectCatHouse(Model model){
         
        List<CatHouse> catHouse = cinemaService.selectCatHouse();
        
        System.out.println( model.addAttribute(catHouse));
        
         return catHouse;
     }
*/
   /* *//**
     * 查看电影图片
     *//*
    @RequestMapping("img")
    @ResponseBody
    public String selectImg() {
        List<Movie> listMovie = mDetailService.selectMovies();
        ArrayList<String> arrayList = new ArrayList<>();
        for (Movie movie : listMovie) {
            arrayList.add(movie.getMPic());
        }
        ArrayList<String> arrayList2 = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            arrayList2.add(arrayList.get(i));
        }
        return JSON.toJSONString(arrayList2);
    }*/
    
  /*  *//**
     * 通过电影Id查找相关信息
     *//*
    @RequestMapping("mmm")
    @ResponseBody
    public List<BasePojo> getBaseInfoByMId(Model model,Integer mId) {
         
        List<BasePojo> list = baseService.getBaseInfoByMId(1);
        System.out.println(model.addAttribute(list));
        return list;
         
     }*/

    /**
     * 影厅场次
     */
    @RequestMapping("hall")
    public String hall() {
        
        return "hall";
        
    }
    
   /*
    * 购票成功
    */
    @RequestMapping("success")
    @ResponseBody
    public String buyTicketSuccess(ModelMap map) {
        
        return "buyTicketSuccess";
    }
    
    
}
