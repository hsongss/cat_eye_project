package com.hp.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import com.hp.pojo.Admin;
import com.hp.service.IAdminService;


public class AdminRealm extends AuthorizingRealm{
	@Autowired
	private IAdminService adminService;
    /**
	 * 执行授权逻辑
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		// 给资源进行授权
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

		// 添加资源的授权字符串
//		info.addStringPermission("user:add");
		//到数据库里查询当前登录用户的授权字符串
		//获取当前登录用户
		Subject subject = SecurityUtils.getSubject();
		Admin admin = (Admin) subject.getPrincipal();
		Admin dbAdmin = adminService.findByaId(admin.getAId());
		info.addStringPermission(dbAdmin.getAPerms());
		return info;
	}
	
	
	/**
	 * 执行认证逻辑
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken arg0) throws AuthenticationException {
		System.out.println("执行认证逻辑2"); 
		//假设数据库的用户名和密码
//		String name = "zs";
		
//		String passWord = "123";
		
		//编写shiro判断逻辑，判断用户名和密码
		//1.判断用户名
		UsernamePasswordToken token = (UsernamePasswordToken) arg0;
		
		Admin admin = adminService.findByName(token.getUsername());
		
		if(admin==null) {
		//用户名不存在
		return null;
	}
	return new SimpleAuthenticationInfo(admin, admin.getAPwd(), "");
		
//		if(!token.getUsername().equals(name)) {
//			//用户名不存在
//			return null;
//		}
//		return new SimpleAuthenticationInfo("", passWord, "");
	}

}
